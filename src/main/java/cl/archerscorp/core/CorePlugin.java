package cl.archerscorp.core;

import cl.archerscorp.ArchersCorp;
import cl.archerscorp.core.command.ArcherCommand;
import cl.archerscorp.core.plugin.ArcherPlugin;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class CorePlugin extends ArcherPlugin {

    public static CorePlugin getInstance() {
        return (CorePlugin) ArchersCorp.getPlugin("Core");
    }

    @Override
    public void onLoad() {
        // Load all commands in commands package
        List<ClassLoader> classLoadersList = new LinkedList<>();
        classLoadersList.add(ClasspathHelper.contextClassLoader());
        classLoadersList.add(ClasspathHelper.staticClassLoader());

        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("cl.archerscorp.core.commands"))));

        Set<Class<? extends ArcherCommand>> allClasses = reflections.getSubTypesOf(ArcherCommand.class);
        for (Class<? extends ArcherCommand> obj : allClasses) {
            try {
                ArcherCommand o = obj.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onEnable() {
        // Activate registered plugins
        for (ArcherCommand command : ArchersCorp.getRegisteredCommands()) {
            if (!command.isRegistered())
                try {
                    command.register();
                    log().info("Registered command " + command.getName());
                } catch (IllegalAccessException | NoSuchFieldException e) {
                    e.printStackTrace();
                    log().warn("Failed to register command " + command.getName());
                }
        }
    }

    @Override
    public CorePlugin getCore() {
        return this;
    }
}
