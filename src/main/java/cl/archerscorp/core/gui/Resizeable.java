package cl.archerscorp.core.gui;


public interface Resizeable<T extends GuiElement> {

    T setSize(int height, int width);
}
