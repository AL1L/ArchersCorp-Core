package cl.archerscorp.core.gui;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.awt.*;
import java.util.List;


public abstract class Button extends GuiElement<Button> implements Resizeable {
    private ItemStack item;

    public Button(ItemStack item) {
        this.item = item;
    }

    public ItemStack getItem() {
        return item;
    }

    @Override
    public Button setSize(int height, int width) {
        shape.width = width;
        shape.height = height;
        return this;
    }

    @Override
    public void onPlace(Inventory i) {
        if (isAutoPlace())
            setPosistion(Gui.slotToPoint(i.firstEmpty()), false);
        if (getGui().isAutoSize())
            if (Gui.slotToPoint(getGui().getSize() - 1).y < shape.y || getGui().getSize() == 0) {
                getGui().setSize(Gui.pointToSlot(new Point(8, shape.y)) + 1);
                return;
            }

        List<Integer> slots = Gui.shapeToSlots(shape);
        for (Integer s : slots)
            if (s < getGui().getSize())
                i.setItem(s, item);
    }
}
