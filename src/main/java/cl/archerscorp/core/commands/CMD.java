package cl.archerscorp.core.commands;

import cl.archerscorp.core.CorePlugin;
import cl.archerscorp.core.command.ArcherCmd;
import cl.archerscorp.core.command.ArcherCommand;
import cl.archerscorp.core.command.CommandSenders;
import org.bukkit.ChatColor;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class CMD extends ArcherCommand {

    final List<String> bannedCmds = Arrays.asList("py", "python", "python3", "pause", "timeout");

    public CMD() {
        super(CorePlugin.getInstance(), "CMD");
        setRunAsync(true);
        setAllowedSenders(CommandSenders.CONSOLE);
    }

    @Override
    public void onCommand(ArcherCmd cmd) {
        String[] args = cmd.getArgs();
        if (args.length > 0) {
            if (bannedCmds.contains(args[0].toLowerCase().trim())) {
                cmd.getSender().sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "ERROR!" + ChatColor.RED + " This command is disabled.");
                return;
            }
        }
        try {
            new ProcessBuilder("cmd", "/c", cmd.getAllArgs()).inheritIO().start().waitFor();
            getCore().log().info("Executed successfully.");
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<String> onTabComplete(ArcherCmd cmd) {
        return null;
    }
}
