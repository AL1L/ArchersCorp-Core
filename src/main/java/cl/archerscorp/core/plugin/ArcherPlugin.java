package cl.archerscorp.core.plugin;

import cl.archerscorp.ArchersCorp;
import cl.archerscorp.core.CorePlugin;
import cl.archerscorp.core.chat.Formatter;
import cl.archerscorp.core.command.ArcherCommand;
import cl.archerscorp.core.config.IConfig;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Collection;

public class ArcherPlugin extends JavaPlugin implements Listener {

    private final ArcherLogger log;
    private Collection<IConfig> configs = new ArrayList<>();
    private ChatColor pluginColor;
    private Formatter formatter;
    private Collection<ArcherCommand> commands = new ArrayList<>();

    public ArcherPlugin() {
        pluginColor = ChatColor.AQUA;
        log = new ArcherLogger(this);
        formatter = new Formatter(this);
    }

    public static ArcherLogger log(ArcherPlugin plugin) {
        return plugin.log;
    }

    @Override
    public void onDisable() {
        for (ArcherCommand cmd : commands)
            if (cmd.isRegistered())
                ArchersCorp.unregisterCommand(cmd);
        commands = new ArrayList<>();
    }

    public Collection<ArcherCommand> getCommands() {
        return commands;
    }

    public ChatColor getPluginColor() {
        return pluginColor;
    }

    private void setPluginColor(ChatColor pluginColor) {
        this.pluginColor = pluginColor;
    }

    public CorePlugin getCore() {
        return CorePlugin.getInstance();
    }

    public ArcherLogger log() {
        return log;
    }

    public Formatter format() {
        return formatter;
    }

    public final IConfig getConfig(String name) {
        for (IConfig config : configs) {
            if (config.getName().equals(name)) {
                return config;
            }
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sender.sendMessage(ChatColor.DARK_RED + "Error don't register commands in plugin.yml");
        return false;
    }

    public String getDisplayName() {
        return getName();
    }

    public final Collection<IConfig> getConfigurations() {
        return configs;
    }

    public void registerCommand(ArcherCommand command) {
        commands.add(command);
    }
}
